// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  ApiUrl: 'https://api.themoviedb.org/3/search/movie',
  apiFullMovieURL: 'https://api.themoviedb.org/3/movie/',
  API_TOKEN: '?api_key=984361363348923ad9a01e3ca2604015',

  apiCreditsURL: '/credits',
  apiLanguageFr: '&language=fr',
  apiLanguageFrQuery: '&language=fr&query=',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
