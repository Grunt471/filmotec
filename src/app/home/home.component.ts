
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FilmService } from '../film.service';
import { NgModule } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @NgModule({
    providers: [
      FilmService,
    ],
  })

  isSubmitted = false;
  lesFilms: any = [];
  numPage: number;
  nbPages: number;
  nbOflesFilms: number;
  identForm = new FormGroup({
    title: new FormControl(),
  })

  constructor(private FilmService: FilmService) {
    this.nbOflesFilms = this.lesFilms.length;
  }

  ngOnInit(): void {

  }


  ident() {
    console.log('Données du formulaire...', this.identForm.value);
  }

  recherche() {
    this.isSubmitted = true;
    console.log('Titre saisie : ', this.identForm.value);
    this.FilmService.sendGetRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {
      this.numPage = data['page'];
      this.nbPages = data['total_pages']
      this.FilmService.addInfos(data['results']);
      this.lesFilms = this.FilmService.getAllInfos();
      console.log(data['results']);
    });
  }

  nextPage() {
    this.numPage++;
    this.FilmService.sendGetRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {
      this.FilmService.addInfos(data['results']);
      this.lesFilms = this.FilmService.getAllInfos();
      console.log('deuxieme :', this.numPage);
    });

  }

  previousPage() {
    this.numPage--;

    this.FilmService.sendGetRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {
      this.FilmService.addInfos(data['results']);

      this.lesFilms = this.FilmService.getAllInfos();
      console.log('deuxieme :', this.numPage);
    });

  }
} 
