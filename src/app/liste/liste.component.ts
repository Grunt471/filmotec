import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import  {ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { Film } from '../film';


@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.scss']
})
export class ListeComponent implements OnInit {

  lesFavoris: []=[];

  identForm = new FormGroup({
    comment: new FormControl(),
  })
  
  constructor(private FilmService: FilmService, private activatedRoute: ActivatedRoute) { }

  

  ngOnInit(): void {
    this.lesFavoris = this.FilmService.getAllFavoris();
    console.log(this.lesFavoris);   
  }

  onClicComment(){
    
    console.log("commentaire passe dans la fonction")
    console.log(this.identForm.value.comment)
    
  }

}
