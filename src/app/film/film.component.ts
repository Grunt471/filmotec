import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { ActivatedRoute } from '@angular/router';
import { Film } from '../film';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  lesFilms: any = [];
  crewCredits: any = [];
  castCredits: any = [];
  directorArray: any = [];
  actorsArray: any = [];
  idFilm: number;
  lesFavoris: any = [];
  commentaire: any;
  noteFilm: any = [];
  note: any;
  showFavoriteButton: boolean = true;
  identForm = new FormGroup({
    comment: new FormControl(),
    note: new FormControl(),
  })

  constructor(private FilmService: FilmService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const idURL: number = params['id'];

      this.FilmService.sendGetInformationRequest(idURL).subscribe((data: Object) => {
        this.FilmService.addInfos(Object(data));
        this.lesFilms = this.FilmService.getAllInfos();
        console.log(this.lesFilms);

      });
      this.FilmService.sendGetCreditsRequest(idURL).subscribe((data: Object) => {
        this.FilmService.addInfos(data['crew']);
        this.crewCredits = this.FilmService.getAllInfos();
        this.FilmService.addInfos(data['cast']);
        this.castCredits = this.FilmService.getAllInfos();
        for (let i = 0; i < this.crewCredits.length; i++) {
          if (this.crewCredits[i].job == 'Director') {
            console.log(this.crewCredits[i].name)
            this.directorArray.push([this.crewCredits[i].name]);

          }
        }
        for (let i = 0; i < 5; i++) {
          this.actorsArray.push(this.castCredits[i].name);
        }
        console.log(this.directorArray);
        console.log(this.actorsArray);

        console.log(this.crewCredits);
        console.log(this.castCredits);
        this.lesFavoris = this.FilmService.getAllFavoris();
        console.log(this.lesFavoris);

      });
    });
    this.noteFilm = localStorage.getItem('Note') ? JSON.parse(localStorage.getItem('Note')) : [];

  }


  onClicFavoris(Film: Film) {

    this.activatedRoute.params.subscribe(params => {
      const idURL: number = params['id'];

      this.FilmService.sendGetInformationRequest(idURL).subscribe((data: Object) => {

        this.lesFavoris = this.FilmService.getAllFavoris();



        let bin = true;
        for (let i = 0; i < this.lesFavoris.length; i++) {
          if (this.lesFavoris[i].id == Film.id) {
            bin = false;
            this.FilmService.removeFavoris(Object(data));
          }
        }
        if (bin == true) {
          this.FilmService.addFavoris(Object(data));
        }

      });
    });
    this.showFavoriteButton = false;

  }
  onClicComment() {

    console.log("commentaire passe dans la fonction")
    console.log(this.identForm.value.comment)
    console.log(this.identForm.value.note)
    //todo faire objet note
    this.commentaire = this.identForm.value.comment;
    if (this.identForm.value.note > 5) {

    }
    this.note = this.identForm.value.note;
    localStorage.setItem('Note', JSON.stringify(this.lesFilms.id, this.identForm.value.note, this.identForm.value.comment));


  }
}
