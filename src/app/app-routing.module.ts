import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmComponent } from "./film/film.component";
import { ListeComponent } from "./liste/liste.component";
import { HomeComponent } from "./home/home.component";


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'film/:id', component: FilmComponent },
{ path: 'liste', component: ListeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
